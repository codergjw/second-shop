package cn.wen.dao;

import cn.wen.entity.UserEntity;
import cn.wen.vo.UserGoodsVO;
import cn.wen.vo.UserVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface UserDao extends BaseMapper<UserEntity> {

    /**
     * 查询所有用户
     * @return
     */
    List<UserEntity> queryAllUser();

    /**
     * 赋予角色权限
     */
    void endowRoleToUser(@Param("userId") String userId, @Param("roleId") Integer roleId);


    /**
     * 获取浏览记录的数量
     * @param userId
     * @return
     */
    Long selectBrowsingHistoryCountByUser(@Param("userId") String userId);

    /**
     * 获取用户的基本信息
     * @param userId
     * @return
     */
    UserVO getUserBaseInfoByUserId(@Param("userId") String userId);


}

package cn.wen.dao;

import cn.wen.entity.OrderEntity;
import cn.wen.vo.GoodsOrderVO;
import cn.wen.vo.QueryVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface OrderDao extends BaseMapper<OrderEntity> {

    /**
     * 通过用户id 和 状态值来获取订单的列表
     * @param userId
     * @param orderStatus
     * @return
     */
    List<GoodsOrderVO> selectOrderListByUserId(@Param("userId") String userId,@Param("orderStatus") Integer orderStatus);

    /**
     * 模糊查询
     * @param queryVO
     * @param orderStatus
     * @return
     */
    List<GoodsOrderVO> selectOrderListByKeyword(@Param("queryVO") QueryVO queryVO,@Param("orderStatus") Integer orderStatus);

    /**
     * 获取卖家全部信息  通过订单状态
     * @param userStoreId
     * @param orderStatus
     * @return
     */
    List<GoodsOrderVO> selectOrderListByUserStoreId(@Param("userStoreId") String userStoreId, @Param("orderStatus") Integer orderStatus);

    /**
     * 获取卖家全部信息  这里只获取全部在售的订单
     * @param userStoreId
     * @return
     */
    List<GoodsOrderVO> selectOrderOnSoldByUserStoreId(@Param("userStoreId") String userStoreId);

    /**
     * 模糊查询 (卖家)
     * @param queryVO
     * @param orderStatus
     * @return
     */
    List<GoodsOrderVO> selectOrderListByStoreKeyword(@Param("queryVO")QueryVO queryVO, @Param("orderStatus")Integer orderStatus);

    /**
     * 通过订单Id获取边界类的全部数据
     * @param orderId
     * @return
     */
    GoodsOrderVO selectOrderVoByOrderId(@Param("orderId") String orderId);
}

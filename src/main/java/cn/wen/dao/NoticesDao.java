package cn.wen.dao;

import cn.wen.entity.NoticesEntity;
import cn.wen.vo.QueryVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;


import java.util.*;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface NoticesDao extends BaseMapper<NoticesEntity> {

    /**
     * 批量修改通知状态
     * @param noticeIds
     * @return
     */
    int updateNoticesToOld(@Param("noticeIds") List<Integer> noticeIds);

    /**
     * 获取时间区间内的数据
     * @param queryVO
     * @return
     */
    List<NoticesEntity> getNoticesByBetweenTime(@Param("queryVO") QueryVO queryVO);
}

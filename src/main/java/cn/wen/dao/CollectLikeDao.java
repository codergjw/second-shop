package cn.wen.dao;

import cn.wen.entity.CollectLikeEntity;
import cn.wen.vo.QueryVO;
import cn.wen.vo.UserGoodsVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface CollectLikeDao extends BaseMapper<CollectLikeEntity> {

    /**
     * 通过userId和类型获取列表数据
     * @param userId
     * @param type
     * @return
     */
    List<UserGoodsVO> selectListByUserId(@Param("userId") String userId, @Param("type") Integer type);

    /**
     * 模糊查询 查询收藏
     * @param queryVo
     * @return
     */
    List<UserGoodsVO> selectListByKeyword(@Param("queryVo") QueryVO queryVo);

    /**
     * 通过goods_id获取url
     * @return
     */
    List<String> selectGoodsImageList();

//    /**
//     * 获取浏览记录的数据
//     * @param userId
//     * @return
//     */
//    List<UserGoodsVO> selectBrowsingHistoryByUser(@Param("userId") String userId);

}

package cn.wen.dao;

import cn.wen.entity.ImageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface ImageDao extends BaseMapper<ImageEntity> {

    /**
     * 往图片 物品关系表中存储数据
     * @param imageId
     * @param goodsId
     */
    void insertToImageGoodsRelation(@Param("imageId") Integer imageId, @Param("goodsId") Integer goodsId);
}

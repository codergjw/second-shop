package cn.wen.dao;

import cn.wen.entity.GoodsEntity;
import cn.wen.vo.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface GoodsDao extends BaseMapper<GoodsEntity> {

    /**
     * 获取物品的全部信息 和 照片
     * @param userId
     * @param goodsStatus
     * @return
     */
    List<GoodsVO> getGoodsListByUserIdStatus(@Param("userId") String userId,@Param("goodsStatus") Integer goodsStatus);

    /**
     * 通过物品的类别来获取到物品列表
     * @param goodsTypeId
     * @return
     */
    List<GoodsVO> getGoodsListByTypeId(@Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 通过物品Id获取物品的详细信息
     * @param goodsId
     * @return
     */
    GoodsVO getGoodsInfoByGoodsId(@Param("goodsId") Integer goodsId);

    /**
     * 通过指定用户 指定物品状态的数量
     * @param goodsUserId
     * @param goodsStatus
     * @return
     */
    Integer getGoodsNumByUserIdAndGoodsStatus(@Param("goodsUserId") String goodsUserId,@Param("goodsStatus") Integer goodsStatus);

    /**
     * 用户表 物品关系表
     * @param goodsUserId
     * @param goodsId
     */
    void insertGoodsUserRelation(@Param("userId") String goodsUserId, @Param("goodsId") Integer goodsId);

    /**
     * 存储类别和物品关系表
     * @param type
     * @param goodsId
     */
    void insertGoodsTypeRelation(@Param("type") Integer type,@Param("goodsId") Integer goodsId);

    /**
     * 获取浏览量最多的数据
     * @return
     */
    List<GoodsVO> getGoodsHottestRecommended();

    /**
     * 模糊查询 物品列表
     * @param queryVO
     * @return
     */
    List<GoodsAndUserVO> getGoodsListByKeyword(@Param("queryVO") QueryVO queryVO);

    /**
     * 获取激活的轮播图列表
     * @return
     */
    List<CarouselVO> getCarouselList();

    /**
     * 通过userId 获取全部的分组
     * @param userId
     * @return
     */
    List<String> findGroupByYearMonthDay(@Param("userId") String userId);

    /**
     * 通过UserId和分组查询列表
     * @param monthDay
     * @param userId
     * @return
     */
    List<UserGoodsVO> findGoodsByYearMonthDayAndUserId(@Param("monthDay") String monthDay, @Param("userId") String userId);

    /**
     * 通过userId 和 goodsId 来删除浏览记录
     * @param userId
     * @param goodsId
     * @return
     */
    boolean deleteBrowsByUserIdAndGoodsId(@Param("userId") String userId, @Param("goodsId") String goodsId);
}

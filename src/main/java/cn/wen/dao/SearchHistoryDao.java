package cn.wen.dao;

import cn.wen.entity.SearchHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface SearchHistoryDao extends BaseMapper<SearchHistoryEntity> {

    /**
     * 通过userId获取全部的搜索历史
     * @param userId
     * @return
     */
    List<SearchHistoryEntity> getAllSearchHistoryByUserId(@Param("userId") String userId);
}

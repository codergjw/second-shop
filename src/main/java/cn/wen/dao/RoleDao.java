package cn.wen.dao;

import cn.wen.entity.RoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface RoleDao extends BaseMapper<RoleEntity> {

    Set<String> selectAllRoleNamesByUsername(@Param("account") String account);

}

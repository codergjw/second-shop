package cn.wen.dao;

import cn.wen.entity.GoodsTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface GoodsTypeDao extends BaseMapper<GoodsTypeEntity> {
	
}

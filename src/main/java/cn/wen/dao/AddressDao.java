package cn.wen.dao;

import cn.wen.entity.AddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface AddressDao extends BaseMapper<AddressEntity> {

    /**
     * 通过用户Id获取地址信息
     * @param userId
     * @return
     */
    List<AddressEntity> getAddressByUserId(@Param("userId") String userId);

    /**
     * 存储地址关系表数据
     * @param userId
     * @param addressId
     */
    void saveToRelation(@Param("userId") String userId,@Param("addressId") Integer addressId);

    /**
     * 删除地址关系表数据
     * @param addressId
     */
    void deleteToRelation(@Param("addressId") Integer addressId);

    /**
     * 修改地址状态值
     * @param userId
     * @return
     */
    boolean updateAllAddressToNonDefault(@Param("userId") String userId);
}

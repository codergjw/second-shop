package cn.wen.dao;

import cn.wen.entity.CollectLikeEntity;
import cn.wen.entity.CommentEntity;
import cn.wen.vo.CommentVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface CommentDao extends BaseMapper<CommentEntity> {

    /**
     * 获取楼主的列表
     * @param goodsId
     * @return
     */
    List<CommentVO> findByGoodsIdAndParentCommentNull(@Param("goodsId") Integer goodsId);

    /**
     * 通过commentId 获取评论的点赞数量
     * @param commentId
     * @return
     */
    Integer getLikeNumberById(@Param("commentId") Integer commentId);

    /**
     * 通过userId 和commentId判断当前是否已点赞
     * @param userId
     * @param commentId
     * @return
     */
     CollectLikeEntity isLikeByUserId(@Param("userId") String userId, @Param("commentId") Integer commentId);

    /**
     * 获取当前楼主下的所有回复
     * @param commentId
     * @return
     */
    List<CommentVO> listTreeComment(@Param("commentId") Integer commentId);
}

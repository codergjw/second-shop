package cn.wen.dao;

import cn.wen.entity.PermissionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Mapper
@Component
public interface PermissionDao extends BaseMapper<PermissionEntity> {

    Set<String> selectAllPermissionValueByUsername(@Param("account") String account);
}

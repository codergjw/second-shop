package cn.wen.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.*;

/**
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
public class UserGoodsVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 物品的唯一ID
	 */
	private Integer goodsId;

	/**
	 * 物品的名称
	 */
	private String goodsName;

	/**
	 * 物品的图片
	 */
	private List<String> goodsImage;

	/**
	 * 物品二手价
	 */
	private Double goodsSecondPrice;


	/**
	 * 物品的库存
	 */
	private Integer goodsStock;

	/**
	 * 物品的发布标题
	 */
	private String goodsTitle;

	/**
	 * 物品状态：
	 * 0：下架
	 * 1：审核中
	 * 2：上架
	 * 3：已购
	 */
	private Integer goodsStatus;

	/**
	 * 该物品所属用户ID
	 */
	private String goodsUserId;

}

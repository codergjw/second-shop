package cn.wen.controller;

import cn.wen.common.enums.CollectLike;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.CollectLikeEntity;
import cn.wen.service.CollectLikeService;
import cn.wen.vo.QueryVO;
import cn.wen.vo.UserGoodsVO;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class CollectLikeController {

    @Autowired
    private CollectLikeService collectLikeService;

    /**
     * 点赞列表
     */
    @GetMapping("/user/like_list")
    public ResultEntity likeList(@RequestParam("userId") String userId) {
        List<UserGoodsVO> likes = collectLikeService.getCollectLikeListByUser(userId, CollectLike.LIKE.getType());
        return ResultEntity.ok().put("data", likes);
    }


    /**
     * 收藏列表
     */
    @GetMapping("/user/collect_list")
    public ResultEntity collectList(@RequestParam("userId") String userId) {
        List<UserGoodsVO> likes = collectLikeService.getCollectLikeListByUser(userId, CollectLike.COLLECT.getType());
        return ResultEntity.ok().put("data", likes);
    }


    /**
     * 删除单个数据
     */
    @DeleteMapping("/user/collectlike_delete")
    public ResultEntity deleteCollectLikeById(@RequestParam("collectLikeId") String collectLikeId) {
        boolean result = collectLikeService.deleteCollectLikeById(collectLikeId);
        if (!result) {
            return ResultEntity.error(500, "取消收藏或者点赞失败！");
        }
        return ResultEntity.ok();
    }

    /**
     * 模糊查询
     */
    @GetMapping("/user/collectlike/search")
    public ResultEntity selectListByKeyword(@RequestBody QueryVO queryVo) {
        List<UserGoodsVO> entityList = collectLikeService.selectListByKeyword(queryVo);
        return ResultEntity.ok().put("data", entityList);
    }
    

    /**
     * 通过用户id 和 物品id收藏到用户的列表中
     *
     * @param userId
     * @param goodsId
     * @return
     */
    @PostMapping("/user/append-collect")
    public ResultEntity appendCollect(@RequestParam("userId") String userId, @RequestParam("goodsId") Integer goodsId) {
        // 1、先判断是否存在当前值  如果存在则修改状态值  我们不直接删除
        if (collectLikeService.checkCollectOrLikeExistsDb(userId, goodsId, CollectLike.COLLECT.getType())) {
            // 存储状态值为激活状态
            collectLikeService.updateCollectByUserIdAndGoodsId(userId, goodsId, CollectLike.COLLECT.getType(), 1);
            return ResultEntity.ok();
        }
        collectLikeService.insertCollectByUserIdAndGoodsId(userId, goodsId, CollectLike.COLLECT.getType());
        return ResultEntity.ok();
    }

    /**
     * 将当前的物品取消收藏
     * 这里比较明确 因为数据库一定存在该记录  所以只需要更改当前记录的状态值
     *
     * @param userId
     * @param goodsId
     * @return
     */
    @PostMapping("/user/cancel-collect")
    public ResultEntity cancelCollect(@RequestParam("userId") String userId, @RequestParam("goodsId") Integer goodsId) {
        // 存储状态值为激活状态
        collectLikeService.updateCollectByUserIdAndGoodsId(userId, goodsId, CollectLike.COLLECT.getType(), 0);
        return ResultEntity.ok();
    }

    /**
     * 通过用户id 和 物品id点赞到用户的列表中
     *
     * @param userId
     * @param goodsId
     * @return
     */
    @PostMapping("/user/append-like")
    public ResultEntity appendLike(@RequestParam("userId") String userId, @RequestParam("goodsId") Integer goodsId) {
        // 1、先判断是否存在当前值  如果存在则修改状态值  我们不直接删除
        if (collectLikeService.checkCollectOrLikeExistsDb(userId, goodsId, CollectLike.LIKE.getType())) {
            // 存储状态值为激活状态
            collectLikeService.updateCollectByUserIdAndGoodsId(userId, goodsId, CollectLike.LIKE.getType(), 1);
            return ResultEntity.ok();
        }
        collectLikeService.insertCollectByUserIdAndGoodsId(userId, goodsId, CollectLike.LIKE.getType());
        return ResultEntity.ok();
    }

    /**
     * 将当前的物品取消点赞
     * 这里比较明确 因为数据库一定存在该记录  所以只需要更改当前记录的状态值
     *
     * @param userId
     * @param goodsId
     * @return
     */
    @PostMapping("/user/cancel-like")
    public ResultEntity cancelLike(@RequestParam("userId") String userId, @RequestParam("goodsId") Integer goodsId) {
        // 存储状态值为激活状态
        collectLikeService.updateCollectByUserIdAndGoodsId(userId, goodsId, CollectLike.LIKE.getType(), 0);
        return ResultEntity.ok();
    }


}

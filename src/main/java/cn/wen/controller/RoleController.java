package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.RoleEntity;
import cn.wen.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("web:role:list")
    public ResultEntity list(@RequestParam Map<String, Object> params){
        PageUtils page = roleService.queryPage(params);

        return ResultEntity.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{roleId}")
    @RequiresPermissions("web:role:info")
    public ResultEntity info(@PathVariable("roleId") Integer roleId){
		RoleEntity role = roleService.getById(roleId);

        return ResultEntity.ok().put("role", role);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("web:role:save")
    public ResultEntity save(@RequestBody RoleEntity role){
		roleService.save(role);

        return ResultEntity.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("web:role:update")
    public ResultEntity update(@RequestBody RoleEntity role){
		roleService.updateById(role);

        return ResultEntity.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("web:role:delete")
    public ResultEntity delete(@RequestBody Integer[] roleIds){
		roleService.removeByIds(Arrays.asList(roleIds));

        return ResultEntity.ok();
    }

}

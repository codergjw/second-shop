package cn.wen.controller;

import cn.wen.common.enums.HttpCode;
import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.NoticesEntity;
import cn.wen.service.NoticesService;
import cn.wen.vo.QueryVO;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.*;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class NoticesController {

    @Autowired
    private NoticesService noticesService;


    /**
     * 通过用户Id获取通知信息
     */
    @GetMapping("/notices/user/list")
    public ResultEntity list(@RequestParam("userId") String userId){
        List<NoticesEntity> notices = noticesService.getNoticesListByUserId(userId);
        return ResultEntity.ok().put("data", notices);
    }

    /**
     * 将其状态为已读
     */
    @PostMapping("/notices/user/update")
    public ResultEntity updateNotices(@RequestParam("noticeIds") List<Integer> noticeIds){
        boolean result = noticesService.updateNoticesToOld(noticeIds);
        if (!result) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "更新异常!");
        return ResultEntity.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/notices/user/delete")
    public ResultEntity delete(@RequestParam("noticesIds") List<Integer> noticesIds){
        boolean result = noticesService.deleteNoticesById(noticesIds);
        if (!result) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "删除异常!");
        return ResultEntity.ok();
    }

    /**
     * 模糊查询  通过时间间隔
     * @param queryVO
     * @return
     */
    @GetMapping("/notices/user/like-list")
    public ResultEntity likeNoticesList(@RequestBody QueryVO queryVO) {
        List<NoticesEntity> noticesEntities = noticesService.getNoticesByBetweenTime(queryVO);
        return ResultEntity.ok().put("data", noticesEntities);
    }

}

package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.LogEntity;
import cn.wen.service.LogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web/log")
public class LogController {
    @Autowired
    private LogService logService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("web:log:list")
    public ResultEntity list(@RequestParam Map<String, Object> params){
        PageUtils page = logService.queryPage(params);

        return ResultEntity.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{logId}")
    @RequiresPermissions("web:log:info")
    public ResultEntity info(@PathVariable("logId") Integer logId){
		LogEntity log = logService.getById(logId);

        return ResultEntity.ok().put("log", log);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("web:log:save")
    public ResultEntity save(@RequestBody LogEntity log){
		logService.save(log);

        return ResultEntity.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("web:log:update")
    public ResultEntity update(@RequestBody LogEntity log){
		logService.updateById(log);

        return ResultEntity.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("web:log:delete")
    public ResultEntity delete(@RequestBody Integer[] logIds){
		logService.removeByIds(Arrays.asList(logIds));

        return ResultEntity.ok();
    }

}

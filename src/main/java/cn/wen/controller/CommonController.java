package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.RoleEntity;
import cn.wen.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
public class CommonController {

    /**
     * 列表
     */
    @RequestMapping("/error/overLimitIP")
    public ResultEntity overLimitIP(){
        return ResultEntity.error(403, "访问过于频繁，请您稍后再访问！");
    }


}

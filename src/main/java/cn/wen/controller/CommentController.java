package cn.wen.controller;

import cn.wen.common.enums.HttpCode;
import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.CommentEntity;
import cn.wen.service.CommentService;
import cn.wen.service.GoodsService;
import cn.wen.service.UserService;
import cn.wen.vo.CommentVO;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class CommentController {

    @Autowired
    private CommentService commentService;


    /**
     * 通过物品Id获取当前物品下的全部评论
     * @param goodsId
     * @return
     */
    @GetMapping("/comment/list")
    public ResultEntity goodsComments(@RequestParam("goodsId") Integer goodsId, @RequestParam("userId") String userId) {
        // 获取全部评论 包括回复
        List<CommentVO> comments = commentService.listCommentByGoodsId(goodsId, userId);
        return ResultEntity.ok().put("comments", comments);
    }

    /**
     * 添加评论
     * @param commentEntity
     * @return
     */
    @PostMapping("/comment/insert")
    public ResultEntity insertComment(@RequestBody CommentEntity commentEntity) {
        boolean result = commentService.insertComment(commentEntity);
        if (!result) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "发表评论错误");
        return ResultEntity.ok();
    }
}

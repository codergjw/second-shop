package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.PermissionEntity;
import cn.wen.service.PermissionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web/permission")
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("web:permission:list")
    public ResultEntity list(@RequestParam Map<String, Object> params){
        PageUtils page = permissionService.queryPage(params);

        return ResultEntity.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{permissionsId}")
    @RequiresPermissions("web:permission:info")
    public ResultEntity info(@PathVariable("permissionsId") Integer permissionsId){
		PermissionEntity permission = permissionService.getById(permissionsId);

        return ResultEntity.ok().put("permission", permission);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("web:permission:save")
    public ResultEntity save(@RequestBody PermissionEntity permission){
		permissionService.save(permission);

        return ResultEntity.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("web:permission:update")
    public ResultEntity update(@RequestBody PermissionEntity permission){
		permissionService.updateById(permission);

        return ResultEntity.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("web:permission:delete")
    public ResultEntity delete(@RequestBody Integer[] permissionsIds){
		permissionService.removeByIds(Arrays.asList(permissionsIds));

        return ResultEntity.ok();
    }

}

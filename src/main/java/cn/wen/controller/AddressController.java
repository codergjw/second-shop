package cn.wen.controller;

import cn.wen.common.enums.AddressStatus;
import cn.wen.common.enums.HttpCode;
import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.AddressEntity;
import cn.wen.service.AddressService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.*;


/**
 * 
 *
 * @author yaling
 * @email 932043654@qq.com
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class AddressController {

    @Autowired
    private AddressService addressService;

    /**
     * 通过userId获取地址列表
     */
    @GetMapping("/address/user/list")
    public ResultEntity list(@RequestParam("userId") String userId){
        List<AddressEntity> list = addressService.getUserAddressByUserId(userId);
        return ResultEntity.ok().put("data", list);
    }

    /**
     * 通过地址id获取地址的详情信息
     */
    @RequestMapping("/address/info")
    public ResultEntity info(@RequestParam("addressId") Integer addressId){
		AddressEntity address = addressService.getById(addressId);
        return ResultEntity.ok().put("data", address);
    }

    /**
     * 保存地址信息
     */
    @RequestMapping("/address/save")
    public ResultEntity save(@RequestParam("userId") String userId,@RequestBody AddressEntity address){
        address.setCreateTime(new Date());
        address.setUpdateTime(new Date());
        // 需要判断是否设置为默认地址
        if (address.getAddressStatus() == AddressStatus.DEFAULT.getStatus()) {
            // 当前地址设置为默认地址 所以需要将数据库中的状态 1 设置为状态 2
            addressService.updateAllAddressToNonDefault(userId);
            // 设置完成则可以添加数据
        }
        boolean save = addressService.save(address);
        if (!save) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "添加地址异常！");
        // 将地址添加到关系表中
        addressService.saveToRelation(userId,address.getAddressId());
        return ResultEntity.ok();
    }

    /**
     * 修改地址信息
     */
    @RequestMapping("/address/update")
    public ResultEntity update(@RequestBody AddressEntity address, @RequestParam("userId") String userId){
        address.setUpdateTime(new Date());
        // 判断当前地址是否设置为默认
        if (address.getAddressStatus() == AddressStatus.DEFAULT.getStatus()) {
            // 当前地址设置为默认地址 所以需要将数据库中的状态 1 设置为状态 2
            addressService.updateAllAddressToNonDefault(userId);
            // 设置完成则可以添加数据
        }
        boolean update = addressService.updateById(address);
        if (!update) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "修改地址异常！");
        return ResultEntity.ok();
    }

    /**
     * 通过Id 删除地址信息
     */
    @RequestMapping("/address/delete")
    public ResultEntity delete(@RequestParam("addressId") Integer addressId) {
        boolean delete = addressService.removeById(addressId);
        if (!delete) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "删除地址异常！");
        addressService.deleteToRelation(addressId);
        return ResultEntity.ok();
    }

    /**
     * 将传入的addressId 作为默认地址
     * @param addressId
     * @return
     */
    @Transactional
    @RequestMapping("/address/update/default")
    public ResultEntity updateDefault(@RequestParam("userId") String userId,@RequestParam("addressId") Integer addressId) {
        // 将当前用户下的全部地址的状态值设置为2  然后将addressId 的设置为1  为默认状态
        addressService.updateAllAddressToNonDefault(userId);
        // 然后将传入的addressId 设置为默认
        addressService.updateAddressToDefaultById(addressId);
        return ResultEntity.ok();
    }

}

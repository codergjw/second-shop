package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.SearchHistoryEntity;
import cn.wen.service.SearchHistoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class SearchHistoryController {

    @Autowired
    private SearchHistoryService searchHistoryService;

    /**
     * 通过用户id来获取搜索历史
     * 可以将全部的搜索历史传入
     * 每次搜索时候 判断是否超过多少条 默认删除
     */
    @GetMapping("/search-history/list")
    public ResultEntity list(@RequestParam("userId") String userId){
        // 通过用户id获取搜索历史  需要联表获取
        List<SearchHistoryEntity> searchHistoryList = searchHistoryService.getAllSearchHistoryByUserId(userId);
        return ResultEntity.ok().put("searchHistoryList", searchHistoryList);
    }

}

package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.ImageEntity;
import cn.wen.service.ImageService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web/image")
public class ImageController {
    @Autowired
    private ImageService imageService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("web:image:list")
    public ResultEntity list(@RequestParam Map<String, Object> params){
        PageUtils page = imageService.queryPage(params);

        return ResultEntity.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{imageId}")
    @RequiresPermissions("web:image:info")
    public ResultEntity info(@PathVariable("imageId") Integer imageId){
		ImageEntity image = imageService.getById(imageId);

        return ResultEntity.ok().put("image", image);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("web:image:save")
    public ResultEntity save(@RequestBody ImageEntity image){
		imageService.save(image);

        return ResultEntity.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("web:image:update")
    public ResultEntity update(@RequestBody ImageEntity image){
		imageService.updateById(image);

        return ResultEntity.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("web:image:delete")
    public ResultEntity delete(@RequestBody Integer[] imageIds){
		imageService.removeByIds(Arrays.asList(imageIds));

        return ResultEntity.ok();
    }

}

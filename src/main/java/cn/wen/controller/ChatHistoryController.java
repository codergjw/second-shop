package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.ChatHistoryEntity;
import cn.wen.service.ChatHistoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web/chathistory")
public class ChatHistoryController {
    @Autowired
    private ChatHistoryService chatHistoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("web:chathistory:list")
    public ResultEntity list(@RequestParam Map<String, Object> params){
        PageUtils page = chatHistoryService.queryPage(params);

        return ResultEntity.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{chatHistoryId}")
    @RequiresPermissions("web:chathistory:info")
    public ResultEntity info(@PathVariable("chatHistoryId") Integer chatHistoryId){
		ChatHistoryEntity chatHistory = chatHistoryService.getById(chatHistoryId);

        return ResultEntity.ok().put("chatHistory", chatHistory);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("web:chathistory:save")
    public ResultEntity save(@RequestBody ChatHistoryEntity chatHistory){
		chatHistoryService.save(chatHistory);

        return ResultEntity.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("web:chathistory:update")
    public ResultEntity update(@RequestBody ChatHistoryEntity chatHistory){
		chatHistoryService.updateById(chatHistory);

        return ResultEntity.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("web:chathistory:delete")
    public ResultEntity delete(@RequestBody Integer[] chatHistoryIds){
		chatHistoryService.removeByIds(Arrays.asList(chatHistoryIds));

        return ResultEntity.ok();
    }

}

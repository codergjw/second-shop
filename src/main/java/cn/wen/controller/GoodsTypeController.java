package cn.wen.controller;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.GoodsTypeEntity;
import cn.wen.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.Map;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class GoodsTypeController {
    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 获取全部的物品分类列表
     */
    @GetMapping("/goods-type/list")
    public ResultEntity list(){
        List<GoodsTypeEntity> goodsTypeEntityList = goodsTypeService.getAllGoodsType();
        return ResultEntity.ok().put("goodsTypes", goodsTypeEntityList);
    }



}

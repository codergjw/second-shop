package cn.wen.controller;

import cn.wen.common.enums.CollectLike;
import cn.wen.common.enums.GoodsStatus;
import cn.wen.common.enums.HttpCode;
import cn.wen.common.utils.ResultEntity;
import cn.wen.entity.GoodsEntity;
import cn.wen.service.CollectLikeService;
import cn.wen.service.GoodsService;
import cn.wen.service.ImageService;
import cn.wen.service.UserService;
import cn.wen.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@RestController
@RequestMapping("web")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private UserService userService;

    @Autowired
    private CollectLikeService collectLikeService;

    @Autowired
    private ImageService imageService;


    /**
     * 获取小程序的轮播图 列表
     * @return
     */
    @GetMapping("/carousel/list")
    public ResultEntity carouselList() {
        List<CarouselVO> carouselList = goodsService.getCarouselList();
        return ResultEntity.ok().put("data", carouselList);
    }

    /**
     * 转让--- 发布中  超级用户权限
     * @param userId
     * @return
     */
    @GetMapping("/goods/list/sold-in")
    public ResultEntity soldIn(@RequestParam("userId") String userId) {
        List<GoodsVO> goodsList = goodsService.getGoodsListByUserIdStatus(userId, GoodsStatus.SOLD_IN.getCode());
        return ResultEntity.ok().put("data", goodsList);
    }

    /**
     * 转让--- 已下架  超级用户权限
     * @param userId
     * @return
     */
    @GetMapping("/goods/list/sold-down")
    public ResultEntity soldDown(@RequestParam("userId") String userId) {
        List<GoodsVO> goodsList = goodsService.getGoodsListByUserIdStatus(userId, GoodsStatus.SOLD_DOWN.getCode());
        return ResultEntity.ok().put("data", goodsList);
    }

    /**
     * 转让--- 已转让  超级用户权限
     * @param userId
     * @return
     */
    @GetMapping("/goods/list/sold-out")
    public ResultEntity soldOut(@RequestParam("userId") String userId) {
        List<GoodsVO> goodsList = goodsService.getGoodsListByUserIdStatus(userId, GoodsStatus.SOLD_OUT.getCode());
        return ResultEntity.ok().put("data", goodsList);
    }

    /**
     * 转让--- 待审核  超级用户权限
     * @param userId
     * @return
     */
    @GetMapping("/goods/list/in-review")
    public ResultEntity inReview(@RequestParam("userId") String userId) {
        List<GoodsVO> goodsList = goodsService.getGoodsListByUserIdStatus(userId, GoodsStatus.IN_REVIEW.getCode());
        return ResultEntity.ok().put("data", goodsList);
    }

    /**
     * 转让--- 未通过 超级用户权限
     * @param userId
     * @return
     */
    @GetMapping("/goods/list/not-pass")
    public ResultEntity notPass(@RequestParam("userId") String userId) {
        List<GoodsVO> goodsList = goodsService.getGoodsListByUserIdStatus(userId, GoodsStatus.NOT_PASS.getCode());
        return ResultEntity.ok().put("data", goodsList);
    }

    /**
     * 转让--- 更改状态 超级用户权限
     * goodsId : 物品ID
     * goodsStatus:
     *  0：下架
     *  1：审核中
     *  2：上架
     *  3：已卖出
     *  4：未通过
     * @param goodsId 通过goodsId来更改状态
     * @return
     */
    @PostMapping("/goods/update/goods-status")
    public ResultEntity updateGoodsStatus(@RequestParam("goodsId") Integer goodsId, @RequestParam("goodsStatus") Integer goodsStatus) {
        boolean result = goodsService.updateGoodsStatusByGoodsId(goodsId, goodsStatus);
        if (!result) return ResultEntity.error(HttpCode.内部服务器错误.getCode(), "修改失败");
        return ResultEntity.ok();
    }

    /**
     * 已经卖出的订单
     * @param userId
     * @return
     */
    @GetMapping("/user/goods-saled/list")
    public ResultEntity getGoodsSale(@RequestParam("userId") String userId) {
        List<GoodsVO> list = goodsService.getGoodsListByUserIdStatus(userId, GoodsStatus.SOLD_OUT.getCode());
        return ResultEntity.ok().put("data", list);
    }

    /**
     * 通过goodsTypeId 来获取列表数据
     * 默认传入的无  无的字符就是全部查询
     * @param goodsTypeId
     * @return
     */
    @GetMapping("/index/goods/list")
    public ResultEntity getIndexListByGoodsType(@RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId) {
        List<GoodsVO> list = goodsService.getGoodsListByTypeId(goodsTypeId);
        return ResultEntity.ok().put("data", list);
    }



    /**
     * 通过GoodsId 来获取物品消息
     * 物品信息 包括
     * 1、物品基本信息
     * 2、物品所属用户的信息
     * 3、卖出的宝贝数量
     * @param goodsId
     * @return
     */
    @GetMapping("/index/goods/info")
    public ResultEntity getIndexGoodsById(@RequestParam("userId") String userId,@RequestParam("goodsId") Integer goodsId) {
        // 获取物品的基本信息
        GoodsVO goodsVO = goodsService.getGoodsInfoByGoodsId(goodsId);
        // 通过获取的物品的所属用户Id来获取该店铺信息
        UserVO userBelong = userService.getUserBaseInfoById(goodsVO.getGoodsUserId());
        // 判断当前所属用户卖出宝贝数量
        Integer userSoldOutNum = goodsService.getGoodsNumByUserIdAndGoodsStatus(goodsVO.getGoodsUserId());
        // 判断当前用户是否点赞
        boolean isLike = collectLikeService.checkCollectOrLikeExistsDb(userId, goodsId, CollectLike.LIKE.getType());
        // 判断当前用户是否收藏
        boolean isCollect = collectLikeService.checkCollectOrLikeExistsDb(userId, goodsId, CollectLike.COLLECT.getType());
        return ResultEntity.ok()
                .put("goodInfo", goodsVO)
                .put("userBelong", userBelong)
                .put("userSoldOutNum", userSoldOutNum)
                .put("isLike", isLike)
                .put("isCollect", isCollect);
    }

    /**
     * 发布物品
     * @param goodsVO
     * @return
     */
    @Transactional
    @PostMapping("/goods/addition-goods")
    public ResultEntity additionGoods(@RequestBody GoodsVO goodsVO) {
        // 1、填写物品的基本信息
        Integer goodsId = goodsService.additionGoods(goodsVO);
        // 2、将图片存储到图片表中 同时需要将关系表填写
        imageService.insertImageByGoodsId(goodsVO.getGoodsImage(), goodsVO.getGoodsId());
        // 3、用户表和物品表的关系表填写
        goodsService.insertGoodsUserRelation(goodsVO.getGoodsUserId(), goodsVO.getGoodsId());
        // 4、物品表和分类表的填写  通过分隔符实现 1、2 来分隔
        goodsService.insertGoodsTypeRelation(goodsVO.getGoodsType(), goodsVO.getGoodsId());

        // 因为需要详情  所以这里需要返回一个当前物品的数据
        GoodsEntity goodsEntity = goodsService.getById(goodsId);
        return ResultEntity.ok().put("goodsInfo", goodsEntity);
    }

    /**
     * 获取最热推荐
     * @return
     */
    @GetMapping("/hottest/recommended")
    public ResultEntity getHotGoodsView() {
        // 需要通过热度来排序 同时需要判断当前还存在库存状态
        List<GoodsVO> goodsVOS = goodsService.getGoodsHottestRecommended();
        return ResultEntity.ok().put("goods", goodsVOS);
    }

    /**
     * 模糊查询 获取数据列表
     * @param queryVO
     * @return
     */
    @GetMapping("/goods/search/list")
    public ResultEntity searchListByKeyword(@RequestBody QueryVO queryVO) {
        // 1、物品的基本数据
        List<GoodsAndUserVO> userGoodsVOS = goodsService.selectSearchGoodsByKeyword(queryVO);
        for (GoodsAndUserVO userGoodsVO : userGoodsVOS) {
            // 2、点赞数量
            userGoodsVO.setLikeNum(collectLikeService.getCollectLikeNumByUser(
                    userGoodsVO.getGoodsUserId(), CollectLike.LIKE.getType()));
            // 3、评论数量
            userGoodsVO.setCommentNum(collectLikeService.getCollectLikeNumByUser(
                    userGoodsVO.getGoodsUserId(), CollectLike.COLLECT.getType()));
            // 4、判断用户是否点赞当前物品
            userGoodsVO.setUp(collectLikeService.checkCollectOrLikeExistsDb(userGoodsVO.getGoodsUserId(),
                    userGoodsVO.getGoodsId(), CollectLike.LIKE.getType()));
        }
        return ResultEntity.ok().put("goodsList", userGoodsVOS);
    }




}

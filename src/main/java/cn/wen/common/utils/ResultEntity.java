package cn.wen.common.utils;

import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 */
public class ResultEntity extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public ResultEntity() {
		put("code", 200);
		put("msg", "success");
	}
	
	public static ResultEntity error() {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
	}
	
	public static ResultEntity error(String msg) {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
	}
	
	public static ResultEntity error(int code, String msg) {
		ResultEntity resultEntity = new ResultEntity();
		resultEntity.put("code", code);
		resultEntity.put("msg", msg);
		return resultEntity;
	}

	public static ResultEntity ok(String msg) {
		ResultEntity resultEntity = new ResultEntity();
		resultEntity.put("msg", msg);
		return resultEntity;
	}
	
	public static ResultEntity ok(Map<String, Object> map) {
		ResultEntity resultEntity = new ResultEntity();
		resultEntity.putAll(map);
		return resultEntity;
	}
	
	public static ResultEntity ok() {
		return new ResultEntity();
	}

	public ResultEntity put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}

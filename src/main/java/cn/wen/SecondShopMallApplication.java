package cn.wen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan("cn.wen.config")
@SpringBootApplication
public class SecondShopMallApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondShopMallApplication.class, args);
    }

}

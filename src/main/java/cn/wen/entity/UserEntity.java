package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`user`")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID为8为字符前四位为年月，后4位为自增
	 * 例：22070001
	 */
	@TableId
	private String userId;
	/**
	 * 用户名，由用户自己提供（只能有汉字和英语字符）注册时默认为微信名称
	 */
	private String username;

	/**
	 * 用户密码
	 */
	private String password;

	/**
	 * 用户的性别男/女（存汉字）
	 */
	private String userSex;
	/**
	 * 用户的手机号码（绑定可用于登录账号）
	 */
	private String userPhone;

	/**
	 * 用户的个性签名 16汉字以内
	 */
	private String userSignature;


	/**
	 * 用户的头像链接（OSS存放）
	 */
	private String userAvatar;
	/**
	 * 用户的绑定的微信号
	 */
	private String userWechatOpid;
	/**
	 * 用户的绑定的qq账号
	 */
	private String userWechat;
	/**
	 * 0：正常使用
	 * 1：非法登录冻结状态；
	 * 2：非法操作冻结状态；
	 * 3：逻辑删除状态
	 * 4：禁用
	 */
	private Integer userStatus;
	/**
	 * 用户属于的学校名称
	 */
	private String school;
	/**
	 * 用户属于的学院
	 */
	private String faculty;
	/**
	 * 用户创建账号时间（默认精确到秒）
	 */
	private Date createTime;
	/**
	 * 用户更新账号时间（默认值为精确到秒）
	 */
	private Date updateTime;


	private String nickname;

	/**
	 * 用户所在班级（发布必填）
	 */
	private String userClass;

	/**
	 * 用户发布二手物品必须填写真实学号
	 */
	private String userStudentId;

}

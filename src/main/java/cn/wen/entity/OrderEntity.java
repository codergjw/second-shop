package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`order`")
public class OrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 654+时间戳
	 */
	@TableId
	private String orderId;
	/**
	 * 店铺（用户）ID
	 */
	private String userShopId;
	/**
	 * 物品ID
	 */
	private Integer goodsId;
	/**
	 * 物品数量
	 */
	private Integer goodsNum;
	/**
	 * 卖家地址收件人
	 */
	private String goodsAddressUsername;
	/**
	 * 卖家地址收货联系号码
	 */
	private String goodsAddressPhone;
	/**
	 * 物品所在具体地址
	 */
	private String goodsAddressContent;
	/**
	 * 订购用户ID
	 */
	private String userId;
	/**
	 * 订单总值
	 */
	private Double totalPrice;
	/**
	 * 订单类型：
	 * 1：自提
	 * 2：邮寄
	 */
	private Integer orderType;
	/**
	 * 订单备注
	 */
	private String remark;
	/**
	 * 订单状态：
	 * 1：待付款
	 * 2：待发货
	 * 3：待收货
	 * 4：成功
	 * 5：失败
	 */
	private Integer orderStatus;
	/**
	 * 物流单号
	 */
	private String dvyFlowId;
	/**
	 * 订单运费
	 */
	private Double dvyAmount;
	/**
	 * 地址收件人
	 */
	private String addressUsername;
	/**
	 * 地址收货联系号码
	 */
	private String addressPhone;
	/**
	 * 具体
	 */
	private String addressContent;
	/**
	 * 订单创建时间
	 */
	private Date createTime;
	/**
	 * 订单修改时间。精确到秒
	 */
	private Date updateTime;
	/**
	 * 支付创建时间。精确到秒
	 */
	private Date payTime;
	/**
	 * 发货时间。精确到秒
	 */
	private Date dvyTime;
	/**
	 * 订单完成时间
	 */
	private Date finallyTime;
	/**
	 * 取消时间。精确到秒
	 */
	private Date cancelTime;
	/**
	 * 支付状态：
	 * 0：还未支付
	 * 1：已经支付
	 */
	private Integer isPayed;
	/**
	 * 支付状态：
	 * 0：正常
	 * 1：逻辑删除
	 * 2：永久删除
	 */
	private Integer deleteStatus;
	/**
	 * 订单关闭原因：
	 * 1：支付超时
	 * 2：买家取消
	 */
	private Integer closeType;

}

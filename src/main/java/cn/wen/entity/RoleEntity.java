package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`role`")
public class RoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 按要求给予角色唯一的Id
	 */
	@TableId(value = "role_id", type = IdType.AUTO)
	private Integer roleId;
	/**
	 * （“普通用户”包括买买方， “
	 * 管理员” 管理后台数据）
	 */
	private String roleName;
	/**
	 * 角色具体描述（主要描述角色含有的模块权限且大致的操作权限）
	 */
	private String roleContent;

}

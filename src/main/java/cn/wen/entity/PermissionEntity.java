package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`permission`")
public class PermissionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 按要求给予权限唯一的Id
	 */
	@TableId(value = "permissions_id", type = IdType.AUTO)
	private Integer permissionsId;
	/**
	 * 角色所能够使用的权限模块名称
	 */
	private String permissionsName;
	/**
	 * 本权限拦截器放行路径,
	 * "/admin/**"为管理员模块，"/web/*"为普通用户模块
	 */
	private String permissionsUri;
	/**
	 * 权限的值
	 */
	private String permissionsValue;

	private Integer permissionsStatus;

	private Date createTime;

}

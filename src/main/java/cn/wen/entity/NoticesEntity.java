package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`notices`")
public class NoticesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 通知唯一ID
	 */
	@TableId(value = "notices_id", type = IdType.AUTO)
	private Integer noticesId;
	/**
	 * 通知用户ID
	 */
	private String userId;
	/**
	 * 通知内容
	 */
	private String noticesContent;
	/**
	 * 通知类型：
	 * 1：公告通知
	 * 2：收藏通知
	 * 3：点赞通知
	 * 4：评论通知
	 */
	private Integer noticesType;
	/**
	 * 通知时间（定时任务）定时发送公告之类通知
	 */
	private Date noticesTime;
	/**
	 * 发送通知用户ID（如果为系统公告则为管理员ID）
	 */
	private String noticesHandlerId;
	/**
	 * 创建时间。精确到秒
	 */
	private Date createTime;

}

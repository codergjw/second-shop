package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`address`")
public class AddressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 地址唯一ID
	 */
	@TableId(value = "address_id", type = IdType.AUTO)
	private Integer addressId;
	/**
	 * 收件人姓名
	 */
	private String addressUsername;
	/**
	 * 地址收货联系号码
	 */
	private String addressPhone;

	/**
	 * 地址的地区 江西省 南昌市 青山湖区
	 */
	private String addressRegion;

	/**
	 * 具体地址  江西农业大学东区15栋720宿舍
	 */
	private String addressContent;
	/**
	 * 地址状态：
	 * 0：禁用
	 * 1：默认地址
	 * 2：普通地址
	 */
	private Integer addressStatus;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间。精确到秒
	 */
	private Date updateTime;

}

package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`image`")
public class ImageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 图片库唯一ID
	 */
	@TableId(value = "image_id", type = IdType.AUTO)
	private Integer imageId;
	/**
	 * 日志保存的url
	 */
	private String imageUrl;
	/**
	 * 0：正常
	 * 1：异常
	 */
	private Integer imageStatus;
	/**
	 * 添加时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}

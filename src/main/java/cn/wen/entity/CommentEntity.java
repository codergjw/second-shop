package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`comment`")
public class CommentEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 作为评论信息Id来辨别每条评论信息
	 */
	@TableId(value = "comment_id", type = IdType.AUTO)
	private Integer commentId;

	/**
	 * 用户Id，绑定用户ID 获取信息
	 */
	private String userId;
	/**
	 * 评论内容
	 */
	private String commentContent;
	/**
	 * 评论绑定物品的ID 
	 */
	private Integer goodsId;

	/**
	 * 回复目标ID
	 */
	private Integer parentCommentId;
	/**
	 * 是否为物品所属用户发表的评论
	 * 1: 为客户
	 * 2：发布的用户
	 */
	private Integer goodsComment;
	/**
	 * 是否为店家评论
	 * true：为店家
	 * false：非店家
	 */
	private boolean owner;
	/**
	 * 1：正常使用
	 * 0：逻辑删除
	 */
	private Integer commentStatus;
	/**
	 * 用户评论时间（默认精确到秒）
	 */
	private Date createTime;

	/**
	 * 评论点赞数量
	 */
	private Integer commentLikeNum;


}

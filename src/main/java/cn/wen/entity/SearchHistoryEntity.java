package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`search_history`")
public class SearchHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 搜索历史id
	 */
	@TableId(value = "search_history_id", type = IdType.AUTO)
	private Integer searchHistoryId;
	/**
	 * 搜索历史内容
	 */
	private String searchHistoryText;
	/**
	 * 搜索历史状态：
	 * 0：正常
	 * 1：异常
	 */
	private Integer searchHistoryStatus;
	/**
	 * 搜索数据优先级推荐
	 */
	private Integer searchHistoryPriority;
	/**
	 * 搜索时间
	 */
	private Date createTime;

}

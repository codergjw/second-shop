package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`chat_history`")
public class ChatHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 聊天记录唯一ID
	 */
	@TableId(value = "chat_history_id", type = IdType.AUTO)
	private Integer chatHistoryId;
	/**
	 * 发送信息者
	 */
	private String sendUserId;
	/**
	 * 接收信息者
	 */
	private String reciveUserId;
	/**
	 * 交互内容
	 */
	private String content;
	/**
	 * 该条记录的状态：
	 * 0：删除
	 * 1：正常
	 * 2：逻辑删除
	 */
	private Integer chatHistoryStatus;
	/**
	 * 消息发送时间
	 */
	private Date createTime;

}

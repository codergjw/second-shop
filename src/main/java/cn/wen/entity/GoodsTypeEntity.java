package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`goods_type`")
public class GoodsTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 物品分类id
	 */
	@TableId(value = "goods_type_id", type = IdType.AUTO)
	private Integer goodsTypeId;
	/**
	 * 物品分类名
	 */
	private String goodsTypeName;
	/**
	 * 分类状态：
	 * 0：正常
	 * 1：异常
	 */
	private Integer goodsTypeStatus;

	/**
	 * 分类的图标名
	 */
	private String typeIcon;
	/**
	 * 分类创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间。精确到秒
	 */
	private Date updateTime;

}

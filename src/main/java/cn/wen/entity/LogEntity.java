package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`log`")
public class LogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 日志唯一ID
	 */
	@TableId(value = "log_id", type = IdType.AUTO)
	private Integer logId;
	/**
	 * 日志保存的url
	 */
	private String logUrl;

	/**
	 * 系统类型
	 */
	private String sysType;

	/**
	 * 0：正常
	 * 1：异常
	 */
	private Integer logStatus;
	/**
	 * 日志内容描述
	 */
	private String logContent;
	/**
	 * 用户id的编号
	 */
	private String handerId;
	/**
	 * 用户操作的ip地址
	 */
	private String handerIp;
	/**
	 * 日志创建时间
	 */
	private Date createTime;
	/**
	 * 日志更新时间
	 */
	private Date updateTime;

}

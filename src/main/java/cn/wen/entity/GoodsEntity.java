package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@Getter
@TableName("`goods`")
public class GoodsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 物品的唯一ID
	 */
	@TableId(value = "goods_id", type = IdType.AUTO)
	private Integer goodsId;
	/**
	 * 物品的名称
	 */
	private String goodsName;
	/**
	 * 物品的描述
	 * 限定为300字符以内
	 */
	private String goodsDes;
	/**
	 * 物品原价
	 */
	private Double goodsOriginalPrice;
	/**
	 * 物品二手价
	 */
	private Double goodsSecondPrice;
	/**
	 * 物品的库存
	 */
	private Integer goodsStock;
	/**
	 * 物品的成色（九成新）
	 */
	private Integer goodsOld;
	/**
	 * 物品的发布标题
	 */
	private String goodsTitle;
	/**
	 * 物品上架有效期
	 */
	private Date goodsIndate;
	/**
	 * 物品状态：
	 * 0：下架
	 * 1：审核中
	 * 2：上架
	 * 3：已购
	 */
	private Integer goodsStatus;
	/**
	 * 该物品所属用户填写的联系电话
	 */
	private String goodsPhone;
	/**
	 * 该物品所属用户的联系QQ
	 */
	private String goodsQq;
	/**
	 * 该物品当前所在地址
	 */
	private String goodsAddress;
	/**
	 * 该物品所属用户ID
	 */
	private String goodsUserId;
	/**
	 * 该物品交易类型
	 */
	private String goodsDealType;
	/**
	 * 物品上架时间
	 */
	private Date createTime;
	/**
	 * 物品更新时间
	 */
	private Date updateTime;

	/**
	 * 物品的浏览量
	 */
	private Integer goodsViews;

}

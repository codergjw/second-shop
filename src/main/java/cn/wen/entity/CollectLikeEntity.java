package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
@Data
@TableName("`collect_like`")
public class CollectLikeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 收藏或者点赞唯一ID
	 */
	@TableId(value = "collect_like_id", type = IdType.AUTO)
	private Integer collectLikeId;
	/**
	 * 该收藏或者点赞的拥有者
	 */
	private String userId;
	/**
	 * 收藏或者点赞的物品ID
	 */
	private Integer goodsId;

	/**
	 * 评论Id
	 */
	private Integer commentId;

	/**
	 * 点赞或者收藏类型：
	 * 1：点赞
	 * 2：收藏
	 */
	private Integer collectLikeType;
	/**
	 * 该条记录的状态：
	 * 0：删除
	 * 1：正常
	 * 2：逻辑删除
	 */
	private Integer collectLikeStatus;
	/**
	 * 创建时间。精确到秒
	 */
	private Date createTime;
	/**
	 * 修改时间。精确到秒
	 */
	private Date updateTime;

}

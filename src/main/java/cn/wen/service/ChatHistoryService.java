package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.ChatHistoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface ChatHistoryService extends IService<ChatHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}


package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.RoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;
import java.util.Set;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface RoleService extends IService<RoleEntity> {

    PageUtils queryPage(Map<String, Object> params);

    Set<String> getAllRoleNamesByUsername(String account);
}


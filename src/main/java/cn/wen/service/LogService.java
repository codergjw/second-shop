package cn.wen.service;

import cn.wen.common.enums.Log;
import cn.wen.common.utils.PageUtils;
import cn.wen.common.enums.SysType;
import cn.wen.entity.LogEntity;
import cn.wen.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface LogService extends IService<LogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void log(UserEntity user, HttpServletRequest request, Log log, SysType sysType);
}


package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.SearchHistoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface SearchHistoryService extends IService<SearchHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 联表获取用户的搜索历史
     * @param userId
     * @return
     */
    List<SearchHistoryEntity> getAllSearchHistoryByUserId(String userId);
}


package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.CommentEntity;
import cn.wen.vo.CommentVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface CommentService extends IService<CommentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 通过物品Id获取评论列表
     * @param goodsId
     * @return
     */
    List<CommentVO> listCommentByGoodsId(Integer goodsId, String userId);

    /**
     * 添加评论 回复或者发表评论
     * @param commentEntity
     * @return
     */
    boolean insertComment(CommentEntity commentEntity);
}


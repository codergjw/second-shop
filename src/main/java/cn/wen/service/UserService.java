package cn.wen.service;

import cn.wen.entity.UserEntity;
import cn.wen.vo.BrowsingGoodsVO;
import cn.wen.vo.UserGoodsVO;
import cn.wen.vo.UserVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface UserService extends IService<UserEntity> {

    Map queryPage(Map<String, Object> params);

    /**
     * 通过账号来获取用户数据
     * @param account
     * @return
     */
    UserEntity getUserByAccount(String account);

    /**
     * 通过账号来获取用户数据(不经过redis中间缓存)
     * @param account
     * @return
     */
    UserEntity getUserByAccountDB(String account);

    /**
     * 通过用户手机号来注册用户
     * @param userPhone
     */
    void insert(String userPhone);

    /**
     * 判断账号是否已经注册
     * @param userPhone
     * @return
     */
    boolean checkUserByUserPhone(String userPhone);

    /**
     * 传入user对象修改数据
     * @param userEntity
     */
    boolean updateUser(UserEntity userEntity);


    /**
     * 通过UserId来获取该用户浏览的数据
     * @param userId
     * @return
     */
    Long getBrowsingHistoryNumByUser(String userId);

    /**
     * 通过UserId来获取该用户浏览的数据
     * @param userId
     * @return
     */
    List<BrowsingGoodsVO> getBrowsingHistoryByUser(String userId);

    /**
     * 通过用户Id获取用户的基本信息
     * @param userId
     * @return
     */
    UserVO getUserBaseInfoById(String userId);

    /**
     * 通过userId和goodsId 删除浏览记录
     * @param userId
     * @param goodsId
     * @return
     */
    boolean deleteByUserIdAndGoodsId(String userId, String goodsId);
}


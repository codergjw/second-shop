package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.CollectLikeEntity;
import cn.wen.vo.QueryVO;
import cn.wen.vo.UserGoodsVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface CollectLikeService extends IService<CollectLikeEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取点赞或者收藏的数量
     * @param userId
     * @param type
     * @return
     */
    Long getCollectLikeNumByUser(String userId, Integer type);

    /**
     * 获取点赞或者收藏的列表
     * @param userId
     * @param type
     * @return
     */
    List<UserGoodsVO> getCollectLikeListByUser(String userId, Integer type);

    /**
     * 通过id删除 点赞或者收藏
     * @param collectLikeId
     * @return
     */
    boolean deleteCollectLikeById(String collectLikeId);

    /**
     * 通过模糊查询  查找收藏
     * @param queryVo
     * @return
     */
    List<UserGoodsVO> selectListByKeyword(QueryVO queryVo);

    /**
     * 详情页面收藏物品Id
     * @param userId
     * @param goodsId
     * @return
     */
    boolean insertCollectByUserIdAndGoodsId(String userId, Integer goodsId, Integer type);

    /**
     * 更改收藏或者点赞的状态  如果存在当前数据 则只需要更改状态  0 ： 非启动  1： 启动
     * @param
     * @param
     * @return
     */
    boolean updateCollectByUserIdAndGoodsId(String userId, Integer goodsId, Integer collectLikeType, Integer collectLikeStatus);

    /**
     * 判断当前 物品的点赞或者收藏中存在 存在说明只需要更改状态
     * @param userId
     * @param goodsId
     * @param type
     * @return
     */
    boolean checkCollectOrLikeExistsDb(String userId, Integer goodsId, Integer type);
}


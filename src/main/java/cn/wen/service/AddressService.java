package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.AddressEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface AddressService extends IService<AddressEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 通过用户Id获取全部的地址
     * @param userId
     * @return
     */
    List<AddressEntity> getUserAddressByUserId(String userId);

    /**
     * 将userId 和 addressId 存储到关系表中
     * @param userId
     * @param addressId
     */
    void saveToRelation(String userId, Integer addressId);

    /**
     * 删除关系表中的address_id
     * @param addressId
     */
    void deleteToRelation(Integer addressId);

    /**
     * 将userId下的默认状态的值设置为非默认
     * @param userId
     * @return
     */
    boolean updateAllAddressToNonDefault(String userId);

    /**
     * 将addressId的值设置为默认
     * @param addressId
     */
    void updateAddressToDefaultById(Integer addressId);
}


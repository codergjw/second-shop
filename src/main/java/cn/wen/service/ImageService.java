package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.ImageEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface ImageService extends IService<ImageEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 插入物品表的图片
     * @param goodsImage
     * @param goodsId
     */
    void insertImageByGoodsId(List<String> goodsImage, Integer goodsId);
}


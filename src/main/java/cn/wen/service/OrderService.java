package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.OrderEntity;
import cn.wen.vo.GoodsOrderVO;
import cn.wen.vo.QueryVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取订单数量
     * @param userId
     * @param orderStatus
     * @return
     */
    Long getOrderNumByUser(String userId, Integer orderStatus);

    /**
     * 通过userId 和状态获取
     * @param userId
     * @param orderStatus
     * @return
     */
    List<GoodsOrderVO> getOrderListByUserId(String userId, Integer orderStatus);

    /**
     * 取消订单
     * @param orderId
     * @return
     */
    boolean cancelOrderByOrderId(String orderId);

    /**
     * 确认收货
     * @param orderId
     * @return
     */
    boolean confirmOrderByOrderId(String orderId);

    /**
     * 对退换售后进行
     * @param queryVO
     * @param status
     * @return
     */
    List<GoodsOrderVO> getOrderLikeListByUserId(QueryVO queryVO, Integer status);

    /**
     * 卖家通过订单状态和卖家Id获取列表
     * @param userStoreId
     * @param status
     * @return
     */
    List<GoodsOrderVO> getOrderListByUserStoreId(String userStoreId, Integer status);

    /**
     * 卖家模糊查询
     * @param queryVO
     * @param orderStatus
     * @return
     */
    List<GoodsOrderVO> getOrderLikeListByUserStoreId(QueryVO queryVO, Integer orderStatus);

    /**
     * 下单
     * @param orderEntity
     */
    void placeAnOrder(OrderEntity orderEntity);

    /**
     * 提交订单
     * @param orderEntity
     * @return
     */
    boolean updateOrder(OrderEntity orderEntity);

    /**
     * 支付成功
     * @param orderId
     * @return
     */
    boolean updateOrderById(String orderId);

    /**
     * 获取订单的数据
     * @param orderId
     * @return
     */
    GoodsOrderVO getOrderInfoByOrderId(String orderId);

    /**
     * 获取店铺的全部在售的信息（还未完成的订单）
     * @param userId
     * @return
     */
    List<GoodsOrderVO> selectOrderOnSoldByUserStoreId(String userId);

    /**
     * 通过orderId 来删除订单
     * @param orderId
     * @return
     */
    boolean deleteOrderById(String orderId);
}


package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.PermissionEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;
import java.util.Set;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface PermissionService extends IService<PermissionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 通过用户名获取所有权限码
     * @param account
     * @return
     */
    Set<String> getAllPermissionByUsername(String account);
}


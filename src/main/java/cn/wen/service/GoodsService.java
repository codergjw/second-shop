package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.GoodsEntity;
import cn.wen.entity.UserEntity;
import cn.wen.vo.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface GoodsService extends IService<GoodsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 通过状态值和用户ID获取物品列表
     * @param userId
     * @return
     */
    List<GoodsVO> getGoodsListByUserIdStatus(String userId, Integer goodsStatus);

    /**
     * 通过物品id 来该变当前物品的状态值
     * @param goodsId
     * @param goodsStatus
     * @return
     */
    boolean updateGoodsStatusByGoodsId(Integer goodsId, Integer goodsStatus);

    /**
     * 通过goods的类别获取到物品列表
     * @param goodsTypeId
     * @return
     */
    List<GoodsVO> getGoodsListByTypeId(Integer goodsTypeId);

    /**
     * 通过物品的id获取物品的基本信息
     * @param goodsId
     * @return
     */
    GoodsVO getGoodsInfoByGoodsId(Integer goodsId);

    /**
     * 获取指定用户卖出物品数量
     * @param goodsUserId
     * @return
     */
    Integer getGoodsNumByUserIdAndGoodsStatus(String goodsUserId);

    /**
     * 获取指定的物品的基本信息
     * @param goodsId
     * @return
     */
    GoodsEntity getGoodsBaseInfoByGoodsId(Integer goodsId);

    /**
     * 存入物品的基本信息
     * @param goodsVO
     */
    Integer additionGoods(GoodsVO goodsVO);

    /**
     * 用户表和物品的关系表
     * @param goodsUserId
     * @param goodsId
     */
    void insertGoodsUserRelation(String goodsUserId, Integer goodsId);

    /**
     * 物品表和分类表的关系
     * @param goodsType
     */
    void insertGoodsTypeRelation(String goodsType,Integer goodsId);

    /**
     * 获取系统的物品热度最高同时 状态为上架
     * @return
     */
    List<GoodsVO> getGoodsHottestRecommended();

    /**
     * 通过关键字模糊查询 标题
     * @param queryVO
     * @return
     */
    List<GoodsAndUserVO> selectSearchGoodsByKeyword(QueryVO queryVO);

    /**
     * 获取全部激活的轮播图
     * @return
     */
    List<CarouselVO> getCarouselList();

}


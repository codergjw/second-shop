package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.CollectLikeDao;
import cn.wen.entity.CollectLikeEntity;
import cn.wen.service.CollectLikeService;
import cn.wen.vo.QueryVO;
import cn.wen.vo.UserGoodsVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("collectLikeService")
public class CollectLikeServiceImpl extends ServiceImpl<CollectLikeDao, CollectLikeEntity> implements CollectLikeService {

    @Autowired
    private CollectLikeDao collectLikeDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CollectLikeEntity> page = this.page(
                new Query<CollectLikeEntity>().getPage(params),
                new QueryWrapper<CollectLikeEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public Long getCollectLikeNumByUser(String userId, Integer type) {
        QueryWrapper<CollectLikeEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("collect_like_type", type);
        return collectLikeDao.selectCount(queryWrapper);
    }

    @Override
    public List<UserGoodsVO> getCollectLikeListByUser(String userId, Integer type) {
        // 需要联表
        return collectLikeDao.selectListByUserId(userId, type);
    }

    @Override
    public boolean deleteCollectLikeById(String collectLikeId) {
        QueryWrapper<CollectLikeEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("collect_like_id", collectLikeId);
        return collectLikeDao.delete(queryWrapper) > 0;
    }

    @Override
    public List<UserGoodsVO> selectListByKeyword(QueryVO queryVo) {
        return collectLikeDao.selectListByKeyword(queryVo);
    }

    @Override
    public boolean insertCollectByUserIdAndGoodsId(String userId, Integer goodsId, Integer type) {
        CollectLikeEntity collectLikeEntity = new CollectLikeEntity();
        collectLikeEntity.setCollectLikeStatus(1);
        collectLikeEntity.setCreateTime(new Date());
        collectLikeEntity.setCollectLikeType(type);
        collectLikeEntity.setGoodsId(goodsId);
        collectLikeEntity.setUserId(userId);
        collectLikeEntity.setUpdateTime(new Date());
        return collectLikeDao.insert(collectLikeEntity) >0;
    }

    @Override
    public boolean updateCollectByUserIdAndGoodsId(String userId, Integer goodsId, Integer collectLikeType, Integer collectLikeStatus) {
        // 更改状态
        QueryWrapper<CollectLikeEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .eq("user_id", userId)
                .eq("goods_id", goodsId)
                .eq("collect_like_type",collectLikeType);
        CollectLikeEntity collectLikeEntity = new CollectLikeEntity();
        collectLikeEntity.setCollectLikeStatus(collectLikeStatus);
        return collectLikeDao.update(collectLikeEntity, queryWrapper) >0;
    }


    @Override
    public boolean checkCollectOrLikeExistsDb(String userId, Integer goodsId, Integer type) {
        // 查询是否存在当前记录值 同时判断状态值是否为0
        QueryWrapper<CollectLikeEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .eq("user_id", userId)
                .eq("goods_id", goodsId)
                .eq("collect_like_type",type);
        return collectLikeDao.selectOne(queryWrapper) != null;
    }

}
package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.AddressDao;
import cn.wen.entity.AddressEntity;
import cn.wen.service.AddressService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("addressService")
@Component
public class AddressServiceImpl extends ServiceImpl<AddressDao, AddressEntity> implements AddressService {

    @Autowired
    private AddressDao addressDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AddressEntity> page = this.page(
                new Query<AddressEntity>().getPage(params),
                new QueryWrapper<AddressEntity>()
        );
        page.setTotal(10);
        return new PageUtils(page);
    }

    @Override
    public List<AddressEntity> getUserAddressByUserId(String userId) {
        return addressDao.getAddressByUserId(userId);
    }

    @Override
    public void saveToRelation(String userId, Integer addressId) {
        addressDao.saveToRelation(userId, addressId);
    }

    @Override
    public void deleteToRelation(Integer addressId) {
        addressDao.deleteToRelation(addressId);
    }

    @Override
    public boolean updateAllAddressToNonDefault(String userId) {
        return addressDao.updateAllAddressToNonDefault(userId);
    }

    @Override
    public void updateAddressToDefaultById(Integer addressId) {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setAddressId(addressId);
        addressEntity.setAddressStatus(1);
        addressDao.updateById(addressEntity);
    }

}
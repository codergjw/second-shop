package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.PermissionDao;
import cn.wen.entity.PermissionEntity;
import cn.wen.service.PermissionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;


@Service("permissionService")
public class PermissionServiceImpl extends ServiceImpl<PermissionDao, PermissionEntity> implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PermissionEntity> page = this.page(
                new Query<PermissionEntity>().getPage(params),
                new QueryWrapper<PermissionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public Set<String> getAllPermissionByUsername(String account) {
        return permissionDao.selectAllPermissionValueByUsername(account);
    }

}
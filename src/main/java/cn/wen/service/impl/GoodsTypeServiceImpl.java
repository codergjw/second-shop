package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.GoodsTypeDao;
import cn.wen.entity.GoodsTypeEntity;
import cn.wen.service.GoodsTypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("goodsTypeService")
public class GoodsTypeServiceImpl extends ServiceImpl<GoodsTypeDao, GoodsTypeEntity> implements GoodsTypeService {

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<GoodsTypeEntity> page = this.page(
                new Query<GoodsTypeEntity>().getPage(params),
                new QueryWrapper<GoodsTypeEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<GoodsTypeEntity> getAllGoodsType() {
        return goodsTypeDao.selectList(null);
    }

}
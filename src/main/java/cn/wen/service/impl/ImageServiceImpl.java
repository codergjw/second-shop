package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.ImageDao;
import cn.wen.entity.ImageEntity;
import cn.wen.service.ImageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("imageService")
public class ImageServiceImpl extends ServiceImpl<ImageDao, ImageEntity> implements ImageService {

    @Autowired
    private ImageDao imageDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ImageEntity> page = this.page(
                new Query<ImageEntity>().getPage(params),
                new QueryWrapper<ImageEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void insertImageByGoodsId(List<String> goodsImage, Integer goodsId) {
        // 1、先将数据存入图片库
        for (String url : goodsImage) {
            ImageEntity imageEntity = new ImageEntity();
            imageEntity.setCreateTime(new Date());
            imageEntity.setImageStatus(1);
            imageEntity.setImageUrl(url);
            imageEntity.setUpdateTime(new Date());
            imageDao.insert(imageEntity);
            // 通过goodsId 存储到关系表中
            imageDao.insertToImageGoodsRelation(imageEntity.getImageId(), goodsId);
        }
    }

}
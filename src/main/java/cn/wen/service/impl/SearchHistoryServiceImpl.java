package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.SearchHistoryDao;
import cn.wen.entity.SearchHistoryEntity;
import cn.wen.service.SearchHistoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("searchHistoryService")
public class SearchHistoryServiceImpl extends ServiceImpl<SearchHistoryDao, SearchHistoryEntity> implements SearchHistoryService {

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SearchHistoryEntity> page = this.page(
                new Query<SearchHistoryEntity>().getPage(params),
                new QueryWrapper<SearchHistoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SearchHistoryEntity> getAllSearchHistoryByUserId(String userId) {
        return searchHistoryDao.getAllSearchHistoryByUserId(userId);
    }

}
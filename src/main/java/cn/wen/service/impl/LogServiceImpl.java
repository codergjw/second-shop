package cn.wen.service.impl;

import cn.wen.common.enums.Log;
import cn.wen.common.enums.SysType;
import cn.wen.common.utils.*;
import cn.wen.dao.LogDao;
import cn.wen.entity.LogEntity;
import cn.wen.entity.UserEntity;
import cn.wen.service.LogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Service("logService")
public class LogServiceImpl extends ServiceImpl<LogDao, LogEntity> implements LogService {

    @Autowired
    private LogDao logDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<LogEntity> page = this.page(
                new Query<LogEntity>().getPage(params),
                new QueryWrapper<LogEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void log(UserEntity user, HttpServletRequest request, Log log, SysType sysType) {
        LogEntity logEntity = LogUtils.instanceLog(user, request, log, sysType);
        logDao.insert(logEntity);
    }

}
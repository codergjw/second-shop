package cn.wen.service.impl;

import cn.wen.common.enums.GoodsStatus;
import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.GoodsDao;
import cn.wen.entity.GoodsEntity;
import cn.wen.service.GoodsService;
import cn.wen.vo.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("goodsService")
public class GoodsServiceImpl extends ServiceImpl<GoodsDao, GoodsEntity> implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<GoodsEntity> page = this.page(
                new Query<GoodsEntity>().getPage(params),
                new QueryWrapper<GoodsEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<GoodsVO> getGoodsListByUserIdStatus(String userId, Integer goodsStatus) {
        return goodsDao.getGoodsListByUserIdStatus(userId, goodsStatus);
    }

    @Override
    public boolean updateGoodsStatusByGoodsId(Integer goodsId, Integer goodsStatus) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setGoodsStatus(goodsStatus);
        goodsEntity.setGoodsId(goodsId);
        return goodsDao.updateById(goodsEntity) > 0;
    }

    @Override
    public List<GoodsVO> getGoodsListByTypeId(Integer goodsTypeId) {
        return goodsDao.getGoodsListByTypeId(goodsTypeId);
    }

    @Override
    public GoodsVO getGoodsInfoByGoodsId(Integer goodsId) {
        return goodsDao.getGoodsInfoByGoodsId(goodsId);
    }

    @Override
    public Integer getGoodsNumByUserIdAndGoodsStatus(String goodsUserId) {
        // 通过判断用户拥有的物品  同时判断物品状态为SOLD_OUT状态
        return goodsDao.getGoodsNumByUserIdAndGoodsStatus(goodsUserId, GoodsStatus.SOLD_OUT.getCode());
    }

    @Override
    public GoodsEntity getGoodsBaseInfoByGoodsId(Integer goodsId) {
        return goodsDao.selectById(goodsId);
    }

    @Override
    public Integer additionGoods(GoodsVO goodsVO) {
        GoodsEntity goodsEntity = new GoodsEntity();
        BeanUtils.copyProperties(goodsVO, goodsEntity);
        goodsEntity.setCreateTime(new Date());
        goodsEntity.setGoodsStatus(1);
        goodsEntity.setGoodsViews(0);
        goodsEntity.setUpdateTime(new Date());
        System.out.println(goodsEntity);
        goodsDao.insert(goodsEntity);
        System.out.println(goodsEntity.getGoodsId() +  "打印Id");
        return goodsEntity.getGoodsId();
    }

    @Override
    public void insertGoodsUserRelation(String goodsUserId, Integer goodsId) {
        goodsDao.insertGoodsUserRelation(goodsUserId, goodsId);
    }

    @Override
    public void insertGoodsTypeRelation(String goodsType, Integer goodsId) {
        String[] goodType = goodsType.split("、");
        for (String typeString : goodType) {
            Integer type = Integer.parseInt(typeString);
            // 判断当前数据库存在该id不
            goodsDao.insertGoodsTypeRelation(type, goodsId);
        }

    }

    @Override
    public List<GoodsVO> getGoodsHottestRecommended() {
        List<GoodsVO> goodsVOS = goodsDao.getGoodsHottestRecommended();
        return goodsVOS;
    }

    @Override
    public List<GoodsAndUserVO> selectSearchGoodsByKeyword(QueryVO queryVO) {
        List<GoodsAndUserVO> goodsAndUserVOS = goodsDao.getGoodsListByKeyword(queryVO);
        return goodsAndUserVOS;
    }

    @Override
    public List<CarouselVO> getCarouselList() {
        return goodsDao.getCarouselList();
    }

}
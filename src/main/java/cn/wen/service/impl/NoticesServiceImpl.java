package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.NoticesDao;
import cn.wen.entity.NoticesEntity;
import cn.wen.service.NoticesService;
import cn.wen.vo.QueryVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;


@Service("noticesService")
public class NoticesServiceImpl extends ServiceImpl<NoticesDao, NoticesEntity> implements NoticesService {

    @Autowired
    private NoticesDao noticesDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<NoticesEntity> page = this.page(
                new Query<NoticesEntity>().getPage(params),
                new QueryWrapper<NoticesEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public Long getNoticesNewNumByUser(String userId, Integer noticesStatus) {
        QueryWrapper<NoticesEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("notices_status", noticesStatus);
        return noticesDao.selectCount(queryWrapper);
    }

    @Override
    public List<NoticesEntity> getNoticesListByUserId(String userId) {
        QueryWrapper<NoticesEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return noticesDao.selectList(queryWrapper);
    }

    @Override
    public boolean updateNoticesToOld(List<Integer> noticeIds) {
        int result = noticesDao.updateNoticesToOld(noticeIds);
        return result >0;
    }

    @Override
    public boolean deleteNoticesById(List<Integer> noticesIds) {
        int result = noticesDao.deleteBatchIds(noticesIds);
        return result > 0;
    }

    @Override
    public List<NoticesEntity> getNoticesByBetweenTime(QueryVO queryVO) {
        return noticesDao.getNoticesByBetweenTime(queryVO);

    }

}
package cn.wen.service.impl;

import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.ChatHistoryDao;
import cn.wen.entity.ChatHistoryEntity;
import cn.wen.service.ChatHistoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("chatHistoryService")
public class ChatHistoryServiceImpl extends ServiceImpl<ChatHistoryDao, ChatHistoryEntity> implements ChatHistoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ChatHistoryEntity> page = this.page(
                new Query<ChatHistoryEntity>().getPage(params),
                new QueryWrapper<ChatHistoryEntity>()
        );

        return new PageUtils(page);
    }

}
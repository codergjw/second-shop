package cn.wen.service.impl;

import cn.wen.common.enums.GoodsStatus;
import cn.wen.common.enums.OrderStatus;
import cn.wen.common.utils.PageUtils;
import cn.wen.common.utils.Query;
import cn.wen.dao.OrderDao;
import cn.wen.entity.GoodsEntity;
import cn.wen.entity.OrderEntity;
import cn.wen.service.GoodsService;
import cn.wen.service.OrderService;
import cn.wen.vo.GoodsOrderVO;
import cn.wen.vo.QueryVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private GoodsService goodsService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public Long getOrderNumByUser(String userId, Integer orderStatus) {
        // 通过userID 和 orderStatus 来获取数量
        QueryWrapper<OrderEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        queryWrapper.eq("order_status", orderStatus);
        Long count = orderDao.selectCount(queryWrapper);
        return count;
    }

    @Override
    public List<GoodsOrderVO> getOrderListByUserId(String userId, Integer orderStatus) {
        return orderDao.selectOrderListByUserId(userId, orderStatus);
    }

    @Override
    public boolean cancelOrderByOrderId(String orderId) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderId(orderId);
        orderEntity.setOrderStatus(OrderStatus.CANCEL_ORDER.getStatus());
        int delete = orderDao.updateById(orderEntity);
        return delete > 0;
    }

    @Override
    public boolean confirmOrderByOrderId(String orderId) {
        QueryWrapper<OrderEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderId(orderId);
        orderEntity.setOrderStatus(4);
        return orderDao.update(orderEntity, queryWrapper) > 0;
    }

    @Override
    public List<GoodsOrderVO> getOrderLikeListByUserId(QueryVO queryVO, Integer status) {
        return orderDao.selectOrderListByKeyword(queryVO, status);
    }

    @Override
    public List<GoodsOrderVO> getOrderListByUserStoreId(String userStoreId, Integer status) {
        return orderDao.selectOrderListByUserStoreId(userStoreId, status);
    }

    @Override
    public List<GoodsOrderVO> getOrderLikeListByUserStoreId(QueryVO queryVO, Integer orderStatus) {
        return orderDao.selectOrderListByStoreKeyword(queryVO, orderStatus);
    }

    @Override
    public void placeAnOrder(OrderEntity orderEntity) {
        orderDao.insert(orderEntity);
    }

    @Override
    public boolean updateOrder(OrderEntity orderEntity) {
        QueryWrapper<OrderEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderEntity.getOrderId());
        return orderDao.update(orderEntity, queryWrapper) > 0;
    }

    @Override
    public boolean updateOrderById(String orderId) {
        OrderEntity orderDb = orderDao.selectById(orderId);
        QueryWrapper<OrderEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderStatus(2);
        orderEntity.setUpdateTime(new Date());
        orderEntity.setIsPayed(1);
        orderEntity.setPayTime(new Date());
        // 需要实现同步操作
        int update = 0;
        synchronized (OrderService.class) {
            GoodsEntity goods = goodsService.getById(orderDb.getGoodsId());
            if (goods.getGoodsStock() <= 0) return false;
            update = orderDao.update(orderEntity, queryWrapper);
            // 获取物品的ID  需要判断当前库存  如果库存>1  则不需要下架
            if (goods.getGoodsStock() > 1) {
                goods.setGoodsStock(goods.getGoodsStock() - 1);
                goodsService.updateById(goods);
            } else {
                goods.setGoodsStock(goods.getGoodsStock() - 1);
                goods.setGoodsStatus(GoodsStatus.SOLD_DOWN.getCode());
            }
            goodsService.updateById(goods);
        }
        return update > 0;
    }

    @Override
    public GoodsOrderVO getOrderInfoByOrderId(String orderId) {
        GoodsOrderVO orderVO = orderDao.selectOrderVoByOrderId(orderId);
        return orderVO;
    }

    @Override
    public List<GoodsOrderVO> selectOrderOnSoldByUserStoreId(String userId) {
        return orderDao.selectOrderOnSoldByUserStoreId(userId);
    }

    @Override
    public boolean deleteOrderById(String orderId) {
        int delete = orderDao.deleteById(orderId);
        return delete > 0;
    }

}
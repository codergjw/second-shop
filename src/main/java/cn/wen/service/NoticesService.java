package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.NoticesEntity;
import cn.wen.vo.QueryVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface NoticesService extends IService<NoticesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 通过用户Id获取最新Id
     * @param userId
     * @param noticesStatus
     * @return
     */
    Long getNoticesNewNumByUser(String userId, Integer noticesStatus);

    /**
     * 通过用户Id用户通知信息
     * @param userId
     * @return
     */
    List<NoticesEntity> getNoticesListByUserId(String userId);

    /**
     * 将通知状态设置为已读状态
     * @param noticeIds
     * @return
     */
    boolean updateNoticesToOld(List<Integer> noticeIds);

    /**
     * 删除通知信息
     * @param noticesIds
     * @return
     */
    boolean deleteNoticesById(List<Integer> noticesIds);

    /**
     * 通过时间间隔来获取区间内的数据
     * @param queryVO
     * @return
     */
    List<NoticesEntity> getNoticesByBetweenTime(QueryVO queryVO);
}


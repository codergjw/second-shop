package cn.wen.service;

import cn.wen.common.utils.PageUtils;
import cn.wen.entity.GoodsTypeEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author yaling
 * @email 932043654
 * @date 2022-08-01 21:52:40
 */
public interface GoodsTypeService extends IService<GoodsTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取全部的物品分类
     * @return
     */
    List<GoodsTypeEntity> getAllGoodsType();

}

